//
//  TambahTransaksiViewController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 20/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import CoreData
import RSSelectionMenu

class TambahTransaksiViewController: UIViewController {

    @IBOutlet weak var dateTransaction: UITextField!
    @IBOutlet weak var balanceTransaction: UITextField!
    @IBOutlet weak var typeTransaction: UITextField!
    @IBOutlet weak var formFirstTransactionLabel: UILabel!
    @IBOutlet weak var formFirstTransactionField: UITextField!
    @IBOutlet weak var formSecondTransactionLabel: UILabel!
    @IBOutlet weak var formSecondTransactionField: UITextField!
    @IBOutlet weak var infoTransaction: UITextField!
    
    var selectedTypeTransaction = "Pengeluaran"
    var selectedFirstTransaction = ""
    var selectedSecondTransaction = ""
    private var datePickerVw: UIDatePicker?
    
    var akunArray = [AkunCoreData]()
    var tfPengeluaran = [PengeluaranCoreData]()
    var tfPemasukan = [PendapatanCoreData]()
    
    let context = (UIApplication.shared.delegate as! AppDelegate) .persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        // Override nav bar buttons
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Batal", style: .done, target: self, action: #selector(self.dismissTransaction(sender:)))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "?", style: .done, target: self, action: #selector(self.showGuide(sender:)))
        
        // Gesture Recognizers for dismiss keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        // Features logic
        //budgetTypeExpenseTransaction.placeholder = "Tidak Tersedia"
        datePickerVw = UIDatePicker()
        datePickerVw?.datePickerMode = .date
        datePickerVw?.addTarget(self, action: #selector(TambahTransaksiViewController.dateChanged(datePicker:)), for: .valueChanged)
        dateTransaction.inputView = datePickerVw
        
        getCurrentDate()
        loadAkunCoreDataItems()
        typeTransaction.text = selectedTypeTransaction
        formFirstTransactionLabel.text = "PENGELUARAN UNTUK"
        formFirstTransactionField.placeholder = "Tentukan untuk apa pengeluaran ini"
        formSecondTransactionLabel.text = "DIAMBIL DARI"
        formSecondTransactionField.placeholder = "Tentukan diambil dari mana"
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.view.endEditing(true)
    }
    
    @objc func dismissTransaction(sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    func loadAkunCoreDataItems(){
        let request: NSFetchRequest<AkunCoreData> = AkunCoreData.fetchRequest()
        do {
            akunArray = try context.fetch(request)
        } catch  {
            print("Gagal mendapatkan data dari AkunCoreData")
        }
    }
    
    func getCurrentDate() {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd/MM/yyyy"
        let now = dateformatter.string(from: NSDate() as Date)
        dateTransaction.text = now
    }
    
    @objc func showGuide(sender: AnyObject) {
        print("tutorial terpanggil")
    }
    
    @objc func dateChanged(datePicker: UIDatePicker ){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateTransaction.text = dateFormatter.string(from: datePicker.date)
        //tglTransaksi =  datePicker.date
        view.endEditing(true)
    }
    
    func showAlert() {
        var alertTitle: String = ""
        var alertMsg: String = ""
        
        if selectedTypeTransaction == "Pemasukan" {
            alertTitle = "Tidak Tersedia"
            alertMsg = "Pilihan ini tidak tersedia pada tipe transaksi PEMASUKAN"
        } else if selectedTypeTransaction == "" {
            alertTitle = "Tidak Tersedia"
            alertMsg = "Harap untuk memilih tipe transaksi terlebih dahulu"
            
        }
        
        let alert = UIAlertController(title: alertTitle, message: alertMsg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func typeSelection() {
        let availableType = ["Pengeluaran", "Pemasukan"]
        var selectedType: [String] = []
        
        let menu = RSSelectionMenu(dataSource: availableType) { (cell, name, indexPath) in
            cell.textLabel?.text = name
        }
        
        menu.setSelectedItems(items: selectedType) { (name, index, selected, selectedItems) in
            selectedType = selectedItems
            self.typeTransaction.text = selectedType[0]
            self.selectedTypeTransaction = selectedType[0]
            
            if self.selectedTypeTransaction == "Pemasukan" {
                self.formFirstTransactionLabel.text = "PENDAPATAN DARI"
                self.formFirstTransactionField.placeholder = "Tentukan sumber pemasukan ini"
                self.formSecondTransactionLabel.text = "DIMASUKAN UNTUK"
                self.formSecondTransactionField.placeholder = "Tentukan untuk apa pemasukan ini"
            } else if self.selectedTypeTransaction == "Pengeluaran" {
                self.formFirstTransactionLabel.text = "PENGELUARAN UNTUK"
                self.formFirstTransactionField.placeholder = "Tentukan untuk apa pengeluaran ini"
                self.formSecondTransactionLabel.text = "DIAMBIL DARI"
                self.formSecondTransactionField.placeholder = "Tentukan diambil dari mana"
            }
        }
        
        menu.show(style: .alert(title: "Pilih Tipe Transaksi", action: nil, height: nil), from: self)
    }
    
    func formFirstTransactionSelection() {
        var pilihanAkun = [String]()
        
        for a in akunArray {
            pilihanAkun.append(a.namaakun!)
        }
        
        var akunTerpilih: [String] = []
        
        let akun = RSSelectionMenu(dataSource: pilihanAkun){ (cell,name,IndexPath) in
            cell.textLabel?.text = name
        }
        
        akun.setSelectedItems(items: akunTerpilih) { (name,index,selected,selectedItems) in
            akunTerpilih = selectedItems
            self.formFirstTransactionField.text = selectedItems[0]
            self.selectedFirstTransaction = selectedItems[0]
        }
        
        akun.show(style: .alert (title: "Pilih Akun", action: nil,height: nil), from: self)
    }
    
    func formSecondTransactionSelection() {
        var pilihanKas = [String]()
        
        for a in akunArray {
            pilihanKas.append(a.namaakun!)
        }
        
        var kasTerpilih: [String] = []
        
        let kas = RSSelectionMenu(dataSource: pilihanKas) { (cell,name,IndexPath) in
            cell.textLabel?.text = name
        }
        
        kas.setSelectedItems(items: kasTerpilih){
            (name,index,selected,selectedItems) in
            kasTerpilih = selectedItems
            self.formSecondTransactionField.text = selectedItems[0]
            self.selectedSecondTransaction = selectedItems[0]
        }
        
        kas.show(style: .alert (title: "Pilih Kas", action: nil,height: nil), from: self)
    }
    
    func saveTransactionContext(){
        do {
            try context.save()
        } catch  {
            print("Gagal menyimpan transaksi")
        }
    }
    
    @IBAction func selectTransctionTypeAction(_ sender: UIButton) {
        typeSelection()
    }
    
    @IBAction func selectFirstTransactionAction(_ sender: UIButton) {
        formFirstTransactionSelection()
    }
    
    @IBAction func selectSecondTransactionAction(_ sender: UIButton) {
        formSecondTransactionSelection()
    }
    
    @IBAction func saveTransactionAction(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        if selectedTypeTransaction == "Pengeluaran" {
            let newKeluar = PengeluaranCoreData(context: self.context)
            newKeluar.tgltransaksi = dateFormatter.date(from: dateTransaction.text ?? "01/01/1999")
            newKeluar.akunkeluar = selectedFirstTransaction
            newKeluar.dariakun = selectedSecondTransaction
            let jumlah: Int = Int(balanceTransaction.text!) ?? 0
            newKeluar.jumlah = Int32(jumlah)
            newKeluar.jenistransaksi = "expense"
            newKeluar.deskripsi = infoTransaction.text
            newKeluar.tglinput = Date()
            self.tfPengeluaran.append(newKeluar)
            self.saveTransactionContext()
            
            let alert = UIAlertController (title: "Tambah Transaksi", message: "Transaksi Pengeluaran telah disimpan", preferredStyle: .alert)
            
            let simpan = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in })
            
            alert.addAction(simpan)
            present(alert, animated: true, completion: nil)
        } else if selectedTypeTransaction == "Pemasukan" {
            let newMasuk = PendapatanCoreData(context: self.context)
            newMasuk.tgltransaksi = dateFormatter.date(from: dateTransaction.text ?? "01/01/1999")
            newMasuk.akunmasuk = selectedFirstTransaction
            newMasuk.keakunkas = selectedSecondTransaction
            let jumlah2: Int = Int(balanceTransaction.text!) ?? 0
            newMasuk.jumlah = Int32(jumlah2)
            newMasuk.jenistransaksi = "income"
            newMasuk.deskripsi = infoTransaction.text
            newMasuk.tglinput = Date()
            self.tfPemasukan.append(newMasuk)
            self.saveTransactionContext()
            
            let alert = UIAlertController (title: "Tambah Transaksi", message: "Transaksi Pendapatan telah disimpan", preferredStyle: .alert)
            
            let simpan = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in })
            
            alert.addAction(simpan)
            present(alert, animated: true, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
