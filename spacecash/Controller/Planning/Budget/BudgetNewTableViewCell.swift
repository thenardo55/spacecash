//
//  BudgetTableViewCell.swift
//  spacecash
//
//  Created by Thenardo Ardo on 19/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import LinearProgressView

class BudgetNewTableViewCell: UITableViewCell {

    @IBOutlet weak var nameBudget: UILabel!
    @IBOutlet weak var limitBudget: UILabel!
    @IBOutlet weak var budgetProgress: LinearProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
