//
//  LaporanPendapatanViewController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 24/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import CoreData

class LaporanPendapatanViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var pendapatanTableView: UITableView!
    
    var transMasuk = [PendapatanCoreData]()
    
    let context = (UIApplication.shared.delegate as! AppDelegate) .persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        pendapatanTableView.dataSource = self
        pendapatanTableView.delegate = self
        loadTransaksiPendapatan()
    }
    
    func loadTransaksiPendapatan(){
        let request: NSFetchRequest<PendapatanCoreData> = PendapatanCoreData.fetchRequest()
        do {
            transMasuk = try context.fetch(request)
        } catch  {
            print("Gagal mendapatkan data dari PendapatanCoreData")
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transMasuk.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PendapatanCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            as? PendapatanCell else {
                fatalError("The dequened cell is not an instance of intended cell")
        }
        
        let income = transMasuk[indexPath.row]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        cell.dateTransaction.text = dateFormatter.string(from: income.tgltransaksi!)
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = Locale(identifier: "id")
        let balance = formatter.string(from: NSNumber(value: income.jumlah))
        cell.incomeValue.text = "Rp. \(balance ?? "0")"
        cell.infoTransaction.text = income.deskripsi
        
        return cell
    }

    @IBAction func refreshAction(_ sender: UIButton) {
        loadTransaksiPendapatan()
        pendapatanTableView.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
