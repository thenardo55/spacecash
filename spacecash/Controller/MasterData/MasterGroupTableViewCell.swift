//
//  MasterGroupTableViewCell.swift
//  spacecash
//
//  Created by Thenardo Ardo on 21/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class MasterGroupTableViewCell: UITableViewCell {

    @IBOutlet weak var groupName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
