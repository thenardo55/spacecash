//
//  PlanningViewController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 18/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class PlanningViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Override nav bar buttons
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "?", style: .done, target: self, action: #selector(self.showGuide(sender:)))
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        // Features logic
    }
    
    @objc func showGuide(sender: AnyObject) {
        print("panduan terpanggil")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
