//
//  BudgetNewViewController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 19/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class BudgetNewViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var budgetTable: UITableView!
    
    var budgetList = [BudgetNew]()
    
    func loadDummy() {
        let b1 = BudgetNew(budgetName: "Operasional", budgetAllocated: 40, budgetLimit: 500000)
        let b2 = BudgetNew(budgetName: "Gaji Karyawan", budgetAllocated: 60, budgetLimit: 4000000)
        globalApp.budgetList += [b1, b2]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        budgetTable.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        budgetTable.dataSource = self
        budgetTable.delegate = self
        // Do any additional setup after loading the view.
        loadDummy()
    }
    
    // Functions for table
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return globalApp.budgetList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "BudgetNewTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            as? BudgetNewTableViewCell else {
                fatalError("The dequened cell is not an instance of BudgetTableViewCell")
        }
        
        let thisBudget = globalApp.budgetList[indexPath.row]
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = Locale(identifier: "id")
        let balance = formatter.string(from: NSNumber(value: thisBudget.budgetLimit))
        
        cell.nameBudget.text = thisBudget.budgetName
        cell.limitBudget.text = "Rp. \(balance ?? "")"
        cell.budgetProgress.setProgress(Float(thisBudget.budgetAllocated), animated: true)
        
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
