//
//  HomeViewController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 18/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import CoreData
import LinearProgressView

class HomeViewController: UIViewController {

    @IBOutlet weak var homeMonthAndYear: UILabel!
    @IBOutlet weak var homeBalance: UILabel!
    @IBOutlet weak var homeIncome: UILabel!
    @IBOutlet weak var homeExpense: UILabel!
    @IBOutlet weak var targetBar: LinearProgressView!
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var budgetBar: LinearProgressView!
    @IBOutlet weak var budgetLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    var totalIncome: Int = 0
    var totalExpense: Int = 0
    var overallBalance: Int = 0
    var transMasuk = [PendapatanCoreData]()
    var transKeluar = [PengeluaranCoreData]()
    
    let context = (UIApplication.shared.delegate as! AppDelegate) .persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Override nav bar buttons
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "?", style: .done, target: self, action: #selector(self.showGuide(sender:)))
        
        // Transparent nav bar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        // features logic
        loadAllPendapatan()
        loadAllPengeluaran()
        calculateIncome()
        calculateExpense()
        calculateOverallBalance()
        determineCondition()
    }
    
    @objc func showGuide(sender: AnyObject) {
        print("panduan terpanggil")
    }
    
    func balanceFormatter(balanceValue: Int) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = Locale(identifier: "id")
        let balance = formatter.string(from: NSNumber(value: balanceValue))
        return "Rp. \(balance ?? "0")"
    }
    
    func loadAllPendapatan(){
        let request: NSFetchRequest<PendapatanCoreData> = PendapatanCoreData.fetchRequest()
        do {
            transMasuk = try context.fetch(request)
        } catch  {
            print("Gagal mendapatkan data dari PendapatanCoreData")
        }
    }
    
    func loadAllPengeluaran(){
        let request: NSFetchRequest<PengeluaranCoreData> = PengeluaranCoreData.fetchRequest()
        do {
            transKeluar = try context.fetch(request)
        } catch  {
            print("Gagal mendapatkan data dari PengeluaranCoreData")
        }
    }
    
    func calculateIncome() {
        if transMasuk.count > 0 {
            for i in 0...transMasuk.count-1 {
                totalIncome = totalIncome + Int(transMasuk[i].jumlah)
            }
            homeIncome.text = balanceFormatter(balanceValue: totalIncome)
            globalApp.totalIncomeGl = totalIncome.self
        } else {
            totalIncome = 0
            homeIncome.text = "Rp. \(totalIncome)"
        }
    }
    
    func calculateExpense() {
        if transKeluar.count > 0 {
            for i in 0...transKeluar.count-1 {
                totalExpense = totalExpense + Int(transKeluar[i].jumlah)
            }
            //totalExpense = 6000000
            globalApp.totalExpenseGl = totalExpense.self
            homeExpense.text = balanceFormatter(balanceValue: totalExpense)
        } else {
            totalExpense = 0
            homeExpense.text = "Rp. \(totalExpense)"
        }
    }
    
    func calculateOverallBalance() {
        if totalIncome > 0 || totalExpense > 0 {
            overallBalance = totalIncome - totalExpense
            homeBalance.text = balanceFormatter(balanceValue: overallBalance)
        } else {
            homeBalance.text = "Rp. 0"
        }
    }
    
    func determineCondition() {
        if totalIncome > 0 || totalExpense > 0 {
            if totalIncome > totalExpense {
                infoLabel.text = "Kondisi keuangan Anda baik"
            } else if totalExpense > totalIncome {
                infoLabel.text = "Kondisi keuangan Anda TIDAK baik"
            }
        } else {
            infoLabel.text = "Ayo mulai mencatat transaksi dan mengatur target serta budget Anda"
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
