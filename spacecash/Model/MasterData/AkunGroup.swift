//
//  AkunModel.swift
//  spacecash
//
//  Created by Zaenal Ariifn on 18/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import Foundation

struct ModelAkun {
    var namaAkun: String
    var typeAkun: String
}
