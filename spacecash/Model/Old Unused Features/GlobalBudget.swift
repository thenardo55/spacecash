//
//  GlobalBudget.swift
//  spacecash
//
//  Created by Thenardo Ardo on 11/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class GlobalBudget {
    var globalBudgetMonth: String
    var globalBudgetYear: String
    var globalBudgetAllocated: Int
    var globalBudgetLimit: Int
    
    init(globalMonth: String, globalYear: String, globalAllocated: Int, globalLimit: Int) {
        self.globalBudgetMonth = globalMonth
        self.globalBudgetYear = globalYear
        self.globalBudgetAllocated = globalAllocated
        self.globalBudgetLimit = globalLimit
    }
}
