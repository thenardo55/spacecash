//
//  MasterGroupViewController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 21/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import CoreData

class MasterGroupViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var groupTableView: UITableView!
    
    var groupArray = [GroupCoreData]()
    let context = (UIApplication.shared.delegate as! AppDelegate) .persistentContainer.viewContext
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadItems()
        
        groupTableView.dataSource = self
        groupTableView.delegate = self
        print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func saveItems() {
        do {
            try context.save()
        } catch  {
            print("Gagal menyimpan data ke GroupCoreData")
        }
        groupTableView.reloadData()
    }
    
    func loadItems() {
        let request: NSFetchRequest<GroupCoreData> = GroupCoreData.fetchRequest()
        do {
            groupArray = try context.fetch(request)
        } catch  {
            print("Gagal mendapatkan data dari GroupCoreData")
        }
    }
    
    @IBAction func inputGroupDataActions(_ sender: UIButton) {
        var textField = UITextField()
        
        let alert = UIAlertController (title: "Tambah Group Akun", message: "", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
        let action = UIAlertAction (title: "Tambah", style: .default) { (action) in
            let newGroup = GroupCoreData(context: self.context)
            newGroup.namagroup = textField.text!
            self.groupArray.append(newGroup)
            self.saveItems()
        }
        
        alert.addTextField { ( alertTextField) in alertTextField.placeholder = "Nama Grup Akun"
            print ("Menambahkan Satu Item")
            textField = alertTextField
        }
        
        alert.addAction(action)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "MasterGroupTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            as? MasterGroupTableViewCell else {
                fatalError("The dequened cell is not an instance of BudgetTableViewCell")
        }
        
        let group = groupArray[indexPath.row]
        
        cell.groupName.text = group.namagroup
        
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
