//
//  BudgetNew.swift
//  spacecash
//
//  Created by Thenardo Ardo on 19/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class BudgetNew {
    var budgetName: String
    var budgetAllocated: Int
    var budgetLimit: Int
    
    init(budgetName: String, budgetAllocated: Int, budgetLimit: Int) {
        self.budgetName = budgetName
        self.budgetAllocated = budgetAllocated
        self.budgetLimit = budgetLimit
    }
}
