//
//  BudgetViewController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 10/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import CoreData

class BudgetViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private var fetch: NSFetchedResultsController<BudgetCoreData>!
    
    @IBOutlet weak var budgetTableView: UITableView!
    @IBOutlet weak var nextMonthButton: UIButton!
    @IBOutlet weak var prevMonthButton: UIButton!
    @IBOutlet weak var currMonthAndYear: UILabel!
    @IBOutlet weak var totalBudget: UILabel!
    @IBOutlet weak var totalBudgetBar: UIProgressView!
    
    var budget = [Budget]()
    var currBudget = [Budget]()
    var globalBudget = GlobalBudget(globalMonth: globalApp.currMonth, globalYear: globalApp.currYear, globalAllocated: 0, globalLimit: 0)
    
    func loadInitialBudget() {
        clearBudgetInformation()
        
        let budget1 = Budget(budgetName: "Operasional", budgetAllocated: 950000, budgetLimit: 2500000, budgetMonth: "Juni", budgetYear: "2020")
        let budget2 = Budget(budgetName: "Pengeluaran Mendadak", budgetAllocated: 1100000, budgetLimit: 2000000, budgetMonth: "Juni", budgetYear: "2020")
        let budget3 = Budget(budgetName: "Bahan Utama", budgetAllocated: 1200000, budgetLimit: 3000000, budgetMonth: "Juni", budgetYear: "2020")
        let budget4 = Budget(budgetName: "Operasional", budgetAllocated: 1000000, budgetLimit: 2500000, budgetMonth: "Juli", budgetYear: "2020")
        let budget5 = Budget(budgetName: "Pengeluaran Mendadak", budgetAllocated: 1500000, budgetLimit: 2000000, budgetMonth: "Juli", budgetYear: "2020")
        let budget6 = Budget(budgetName: "Bahan Utama", budgetAllocated: 2300000, budgetLimit: 3000000, budgetMonth: "Juli", budgetYear: "2020")
        
        budget += [budget1, budget2, budget3, budget4, budget5, budget6]
        
        // get budgets from selected month and year
        for i in 0...(budget.count - 1) {
            if globalApp.currMonth == budget[i].budgetMonth {
                currBudget.append(budget[i])
            }
        }
        
        // calculate total allocated and limit budget
        if currBudget.count > 0 {
            for i in 0...(currBudget.count - 1) {
                globalBudget.globalBudgetAllocated = globalBudget.globalBudgetAllocated + currBudget[i].budgetAllocated
                globalBudget.globalBudgetLimit = globalBudget.globalBudgetLimit + currBudget[i].budgetLimit
            }
        }
        
        totalBudget.text = "Rp. \(globalBudget.globalBudgetAllocated) / Rp. \(globalBudget.globalBudgetLimit)"
        
        if currBudget.count == 0 {
            totalBudgetBar.progress = 0.0
        } else {
            totalBudgetBar.progress = Float(globalBudget.globalBudgetAllocated) / Float(globalBudget.globalBudgetLimit)
        }
    }
    
    func clearBudgetInformation() {
        budget.removeAll()
        currBudget.removeAll()
        globalBudget.globalBudgetAllocated = 0
        globalBudget.globalBudgetLimit = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        budgetTableView.dataSource = self
        budgetTableView.delegate = self
        loadInitialBudget()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currBudget.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "BudgetTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            as? BudgetTableViewCell else {
                fatalError("The dequened cell is not an instance of BudgetTableViewCell")
        }
        
        let thisBudget = currBudget[indexPath.row]
        
        cell.budgetName.text = thisBudget.budgetName
        cell.budgetAllocatedPlusLimit.text = "Rp. \(thisBudget.budgetAllocated) / Rp. \(thisBudget.budgetLimit)"
        cell.budgetProgress.progress = (Float(thisBudget.budgetAllocated) / Float(thisBudget.budgetLimit))
       
        return cell
    }
    
    // MARK: - Action
    
    @IBAction func gotoPrevMonth(_ sender: UIButton) {
        if globalApp.currMonth == "Juli" {
            globalApp.currMonth = "Juni"
        } else if globalApp.currMonth == "Agustus" {
            globalApp.currMonth = "Juli"
        } else {
            // nothing happened...
        }
        currMonthAndYear.text = "\(globalApp.currMonth) \(globalApp.currYear)"
        loadInitialBudget()
        budgetTableView.reloadData()
    }
    
    @IBAction func gotoNextMonth(_ sender: UIButton) {
        if globalApp.currMonth == "Juni" {
            globalApp.currMonth = "Juli"
        } else if globalApp.currMonth == "Juli" {
            globalApp.currMonth = "Agustus"
        } else {
            // nothing happened...
        }
        currMonthAndYear.text = "\(globalApp.currMonth) \(globalApp.currYear)"
        loadInitialBudget()
        budgetTableView.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
