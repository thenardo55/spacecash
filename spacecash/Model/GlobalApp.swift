//
//  GlobalApp.swift
//  spacecash
//
//  Created by Thenardo Ardo on 11/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

struct globalApp {
    static var currMonth = "Juni"
    static var currYear = "2020"
    static var totalIncomeGl = 0
    static var totalExpenseGl = 0
    static var budgetList = [BudgetNew]()
}
