//
//  TabController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 13/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class TabController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        // Do any additional setup after loading the view.
        //..s
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController is TransactionNavController {
            if let newVC = tabBarController.storyboard?.instantiateViewController(withIdentifier: "transactionNavController") {
                tabBarController.present(newVC, animated: true)
                return false
            }
        }
        
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
