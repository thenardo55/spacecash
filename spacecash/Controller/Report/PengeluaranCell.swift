//
//  PengeluaranCell.swift
//  spacecash
//
//  Created by Thenardo Ardo on 24/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class PengeluaranCell: UITableViewCell {

    @IBOutlet weak var dateTransaction: UILabel!
    @IBOutlet weak var expenseValue: UILabel!
    @IBOutlet weak var infoTransaction: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
