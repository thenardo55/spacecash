//
//  LaporanPengeluaranViewController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 24/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import CoreData

class LaporanPengeluaranViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var Pengeluaran: UITableView!
    
    var transKeluar = [PengeluaranCoreData]()
    
    let context = (UIApplication.shared.delegate as! AppDelegate) .persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        Pengeluaran.dataSource = self
        Pengeluaran.delegate = self
        loadTransaksiPengeluaran()
    }
    
    func loadTransaksiPengeluaran(){
        let request: NSFetchRequest<PengeluaranCoreData> = PengeluaranCoreData.fetchRequest()
        do {
            transKeluar = try context.fetch(request)
        } catch  {
            print("Gagal mendapatkan data dari PengeluaranCoreData")
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transKeluar.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PengeluaranCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            as? PengeluaranCell else {
                fatalError("The dequened cell is not an instance of intended cell")
        }
        
        let expense = transKeluar[indexPath.row]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        cell.dateTransaction.text = dateFormatter.string(from: expense.tgltransaksi!)
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = Locale(identifier: "id")
        let balance = formatter.string(from: NSNumber(value: expense.jumlah))
        cell.expenseValue.text = "Rp. \(balance ?? "0")"
        cell.infoTransaction.text = expense.deskripsi
        
        return cell
    }
    
    @IBAction func refreshAction(_ sender: UIButton) {
        loadTransaksiPengeluaran()
        Pengeluaran.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
