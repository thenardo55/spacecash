//
//  LaporanViewController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 21/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import Charts

class LaporanViewController: UIViewController {

    @IBOutlet weak var charts: PieChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        // Override nav bar buttons
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "?", style: .done, target: self, action: #selector(self.showGuide(sender:)))
        
        chartUpdate()
    }
    
    func chartUpdate() {
        let entry1 = PieChartDataEntry(value:Double(globalApp.totalIncomeGl), label:"Pendapatan")
        let entry2 = PieChartDataEntry(value:Double(globalApp.totalExpenseGl), label:"Pengeluaran")
        let dataSet = PieChartDataSet(entries:[entry1,entry2], label:"")
        dataSet.colors = ChartColorTemplates.colorful()
        let data = PieChartData(dataSet:dataSet)
        self.charts.data = data
    }
    
    @objc func showGuide(sender: AnyObject) {
        print("panduan terpanggil")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
