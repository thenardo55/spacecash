//
//  TambahBudgeViewController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 19/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import RSSelectionMenu

class TambahBudgetNewViewController: UIViewController {

    @IBOutlet weak var limitBudgetField: UITextField!
    @IBOutlet weak var categoryField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Gesture Recognizers for dismiss keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.view.endEditing(true)
    }
    
    func categorySelection() {
        let availableCategory = ["Operasional", "Investasi", "Tak Terduga", "Bahan Baku"]
        var selectedCategory: [String] = []
        
        let menu = RSSelectionMenu(dataSource: availableCategory) { (cell, name, indexPath) in
            cell.textLabel?.text = name
        }
        
        menu.setSelectedItems(items: selectedCategory) { (name, index, selected, selectedItems) in
            selectedCategory = selectedItems
            self.categoryField.text = selectedCategory[0]
        }
        
        menu.show(style: .alert(title: "Pilih Kategory Budget", action: nil, height: nil), from: self)
    }
    
    func checkForm() -> Bool {
        if limitBudgetField.text != "" && categoryField.text != "" {
            return true
        } else {
            return false
        }
    }

    @IBAction func selectBudgetCategoryAction(_ sender: UIButton) {
        categorySelection()
    }
    
    @IBAction func saveNewBudgetAction(_ sender: UIButton) {
        let isFormFilled: Bool = checkForm()
        print(isFormFilled)
        if isFormFilled {
            let newBudget = BudgetNew(budgetName: categoryField.text!, budgetAllocated: 0, budgetLimit: Int(limitBudgetField.text!) ?? 0)
            globalApp.budgetList.append(newBudget)
            navigationController?.popViewController(animated: true)
        } else {
            print("TAMBAH BUDGET | Form belum lengkap")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
