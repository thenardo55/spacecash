//
//  OnboardViewController.swift
//  spacecash
//
//  Created by Kevin Josal on 24/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class OnboardViewController: UIViewController {
    
    @IBOutlet weak var labelTitle1: UILabel!
    @IBOutlet weak var labelTitle2: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var imageHeader: UIImageView!
    @IBOutlet weak var imageFooter: UIImageView!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonSkip: UIButton!
    
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageHeader.image = #imageLiteral(resourceName: "header_onboard")
        imageFooter.image = #imageLiteral(resourceName: "footer_onboard")
        
        buttonNext.backgroundColor = UIColor(displayP3Red: 44.0/255.0, green: 173.0/255.0, blue: 179.0/255.0, alpha: 100.0)
        buttonNext.layer.cornerRadius = 25

        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        count+=1
        pageControl.currentPage = count
        
        if(count == 1)
        {
            labelTitle1.text = "CAPAI TARGET BISNIS"
            labelTitle2.text = "BULANANMU"
        }
        else if (count == 2)
        {
            labelTitle1.text = "ATUR BUDGET UNTUK"
            labelTitle2.text = "SETIAP PENGELUARAN BISNISMU"
            
        }
        else
        {
            labelTitle1.text = ""
            labelTitle2.text = "LAPORAN"
            buttonSkip.isHidden = true
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
