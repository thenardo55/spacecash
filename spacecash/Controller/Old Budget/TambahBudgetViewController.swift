//
//  TambahBudgetViewController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 10/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import CoreData

class TambahBudgetViewController: UIViewController {
    
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    @IBOutlet weak var nameBudgetTextField: UITextField!
    @IBOutlet weak var targetLimitBudgetField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true // hide tab bar
    }
    
    // Show tab bar after coming back from Tambah Budget
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent {
            self.tabBarController?.tabBar.isHidden = false
        }
    }
    
    func showIncompleteAlert() {
        let msg = UIAlertController(title: "Data kurang lengkap", message: "Harap untuk memenuhi data form budget yang ingin ditambahkan", preferredStyle: .alert)
        let msg_action = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        msg.addAction(msg_action)
        
        present(msg, animated: true, completion: nil)
    }
    
    func saveBudget(name: String, limit: Int) {
        let budget = BudgetCoreData(entity: BudgetCoreData.entity(), insertInto: context)
        
        budget.nameBudget = name
        budget.limitBudget = Int32(limit)
        budget.monthBudget = globalApp.currMonth
        budget.yearBudget = globalApp.currYear
        
        appDelegate.saveContext()
    }
    
    // MARK: - Action

    @IBAction func addBudgetActionButton(_ sender: UIButton) {
        if nameBudgetTextField.text != "" && targetLimitBudgetField.text != "" {
            let nameBudget = nameBudgetTextField.text
            let targetLimit = Int(targetLimitBudgetField.text!)
            print("Nama budget: \(nameBudget ?? ""), Target: \(targetLimit ?? 0)")
            saveBudget(name: nameBudget!, limit: targetLimit ?? 0)
            navigationController?.popViewController(animated: true)
        } else {
            showIncompleteAlert()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
