//
//  BudgetTableViewCell.swift
//  spacecash
//
//  Created by Thenardo Ardo on 10/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class BudgetTableViewCell: UITableViewCell {

    @IBOutlet weak var budgetName: UILabel!
    @IBOutlet weak var budgetType: UILabel!
    @IBOutlet weak var budgetAllocatedPlusLimit: UILabel!
    @IBOutlet weak var budgetProgress: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
