//
//  MasterAkunViewController.swift
//  spacecash
//
//  Created by Thenardo Ardo on 21/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import CoreData
import RSSelectionMenu

class MasterAkunViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var akunTableView: UITableView!
    @IBOutlet weak var namaAkunField: UITextField!
    @IBOutlet weak var grupAkunField: UITextField!
    
    var pickerData: [String] = [String]()
    var groupArray = [GroupCoreData]()
    var akunArray = [AkunCoreData]()
    var namagroupterpilih: String?
    var typeAkunTerpilih: String?
    let context = (UIApplication.shared.delegate as! AppDelegate) .persistentContainer.viewContext
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Gesture Recognizers for dismiss keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        // Feature logic
        akunTableView.dataSource = self
        akunTableView.delegate = self
        loadGroupCoreDataItems()
        loadAkunCoreDataItems()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.view.endEditing(true)
    }
    
    func loadGroupCoreDataItems() {
        let request: NSFetchRequest<GroupCoreData> = GroupCoreData.fetchRequest()
        do {
            groupArray = try context.fetch(request)
        } catch  {
            print("Gagal mendapatkan data dari GroupCoreData")
        }
    }
    
    func loadAkunCoreDataItems() {
        let request: NSFetchRequest<AkunCoreData> = AkunCoreData.fetchRequest()
        do {
            akunArray = try context.fetch(request)
        } catch  {
            print("Gagal mendapatkan data dari AkunCoreData")
        }
    }
    
    func saveAkunItem() {
        do {
            try context.save()
        } catch  {
            print("Gagal menyimpan data ke AkunCoreData")
        }
        akunTableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return akunArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "MasterAkunTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            as? MasterAkunTableViewCell else {
                fatalError("The dequened cell is not an instance of MasterAkunTableViewCell")
        }
        
        let akun = akunArray[indexPath.row]
        
        cell.namaAkun.text = akun.namaakun
        cell.grupAkun.text = akun.typeakun
        
        return cell
    }

    @IBAction func selectGrupAkunAction(_ sender: UIButton) {
        var availableType = [String]()
        
        for i in 0...groupArray.count-1 {
            let availableItem = groupArray[i].namagroup
            availableType.append("\(availableItem ?? "")")
        }
        
        var selectedType: [String] = []
        
        let menu = RSSelectionMenu(dataSource: availableType) { (cell, name, indexPath) in
            cell.textLabel?.text = name
        }
        
        menu.setSelectedItems(items: selectedType) { (name, index, selected, selectedItems) in
            selectedType = selectedItems
            self.grupAkunField.text = selectedType[0]
            
        }
        
        menu.show(style: .alert(title: "Pilih Grup Akun", action: nil, height: nil), from: self)
    }
    
    @IBAction func createAkunBaruAction(_ sender: Any) {
        let newAkun = AkunCoreData(context: self.context)
        newAkun.namaakun = namaAkunField.text
        newAkun.typeakun = grupAkunField.text
        self.akunArray.append(newAkun)
        saveAkunItem()
        
        // Notify user
        let alert = UIAlertController(title: "Berhasil", message: "Akun baru berhasil disimpan", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
