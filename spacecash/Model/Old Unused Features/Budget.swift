//
//  Budget.swift
//  spacecash
//
//  Created by Thenardo Ardo on 10/06/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class Budget {
    var budgetName: String
    var budgetAllocated: Int
    var budgetLimit: Int
    var budgetAllocatedPlusLimit: String
    var budgetMonth: String
    var budgetYear: String
    
    init(budgetName: String, budgetAllocated: Int, budgetLimit: Int, budgetMonth: String, budgetYear: String) {
        self.budgetName = budgetName
        self.budgetAllocated = budgetAllocated
        self.budgetLimit = budgetLimit
        self.budgetAllocatedPlusLimit = "Rp. \(self.budgetAllocated) / Rp. \(self.budgetLimit)"
        self.budgetMonth = budgetMonth
        self.budgetYear = budgetYear
    }
}
